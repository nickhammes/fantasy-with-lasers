from random import randrange;
from math import pi, cos, sin;
import sfml;
import direction;
import enemy;
from vector import Vector;

class AIMoveType:
    """Nonmoving AI."""
    
    def __init__(self, pos = Vector(0,0), moveSpeed = 1):
        self.pos = pos
    
    def update(self, pos):
        self.pos = pos
    
    def name(self):
        return "AIMoveType"
    
    def move(self,session):
        return Vector(0,0)

class AIMoveRandom(AIMoveType):
    """Random motion AI movement."""
    
    def __init__(self, pos = Vector(0,0), moveSpeed = 1):
        self.pos = pos
        self.moveSpeed = moveSpeed
    
    def update(self, pos):
        self.pos = pos
    
    def name(self):
        return "AIMoveRandom"
    
    def move(self, session):
        theta = randrange(0,2)*pi
        distance = self.moveSpeed #session.maxDirectionDistance(theta,moveSpeed)
        return Vector(cos(pi), sin(pi))*distance

class AIUnit:
    def __init__(self, enemyObj, moveType = AIMoveType(), size = 32):
        self.enemyObj = enemyObj
        self._move = moveType
        self._move.update(enemyObj.pos)
        self.size = size
    
    def step(self, session):
        self.enemyObj.step(session)
        self.move(session)
    
    def kill(self, session):
        pass
    
    def move(self, session):
        self._move.update(self.enemyObj.pos)
        self.enemyObj.pos += self._move.move(session)
    
    def getSprite(self):
        self.enemyObj.updateSprite()
        return self.enemyObj.sprite
        
    
    def setSprite(self, filehandle):
        self.enemyObj.sprite = sfml.Sprite(filehandle)
