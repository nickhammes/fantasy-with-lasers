__name__ = "Session"
import sfml
from player import player
from keyconfigs import keyConfigs as KEYS
from constants import *
from ui import *

class Session:
    def __init__(self):
        # Simple Variables
        self.difficulty = HARD #Valid Values: Easy, Medium, Hard, Lunatic, OH GOD WHY
        self.window = sfml.RenderWindow(sfml.VideoMode(1024,768), "Fantasy with Lasers")
        self.window.framerate_limit = 60
        self.paused = False
        # Structured Variables
        self.textures = []
        self.textureNames = []
        self.textures.append(sfml.Texture.from_file("assets/sprites/default.bmp"))
        self.textureNames.append("assets/sprites/default.bmp")
        self.enemies = []
        self.projectiles = []
        self.overlay = Overlay()

        # Class Variables
        self.pc = self.makePlayer()
    # Member Functions
    def makePlayer(self):
        temp = player(0,0,"assets/sprites/player_front.bmp",len(self.textures))
        self.textures.append(sfml.Texture.from_file(temp.defTexture))
        temp.sprite = sfml.Sprite(self.textures[len(self.textures) - 1])
        temp.laser.texture = "assets/sprites/laser.bmp"
        return temp
    def addEnemy(self, AIUnit, texture):
        texID = -1
        for tex in range(0,len(self.textureNames)):
            if self.textureNames[tex] == texture:
                texID = tex
        if (texID == -1):
            self.textures.append(sfml.Texture.from_file(texture))
            texID = (len(self.textures) - 1)
        AIUnit.setSprite(self.textures[texID])
        AIUnit.textureIndex = texID
        AIUnit.enemyID = len(self.enemies)
        self.enemies.append(AIUnit)
    def removeEnemy(self, enemyID):
        self.sprites[self.enemies[enemyID].enemy.spriteIndex].pop()
        self.enemies[enemyID].kill()
        self.enemies.pop(enemyID)
    def addProjectile(self, proj, texture):
        """ 
        Adds the given projectile to the registry.
        
        proj: Projectile to be added.
        texture: File path of the texture of the given projectile.
        """
        texID = -1
        for tex in range(0,len(self.textureNames)):
            if self.textureNames[tex] == texture:
                texID = tex
        if (texID == -1):
            self.textures.append(sfml.Texture.from_file(texture))
            texID = (len(self.textures) - 1)
        proj.setSprite(self.textures[texID])
        proj.textureIndex = texID
        proj.projectileID = len(self.projectiles)
        self.projectiles.append(proj)
    def removeProjectile(self, projectileID):
        self.sprites[self.projectiles[projectileID].projectile.spriteIndex].pop()
        self.projectiles[projectileID].kill()
        self.projectiles.pop(projectileID)
    def step(self):
        if self.keyState("KEY_PAUSE") and not self.paused:
            self.paused = not self.paused
            print("PAUSE STATE FLIPPED")
        if not self.paused:
            for enemy in self.enemies:
                enemy.step(self)
            for projectile in self.projectiles:
                projectile.step(self)
            self.pc.step(self)
            # Draw Objects
            self.window.clear()
            for enemy in self.enemies:
                self.window.draw(enemy.getSprite())
            for projectile in self.projectiles:
                self.window.draw(enemy.getSprite())
            self.overlay.step(self)
            self.window.draw(self.pc.getSprite())
            self.window.display()
        else:
            pass
    def keyState(self, keycode):
        if keycode in KEYS:
            return sfml.Keyboard.is_key_pressed(KEYS[keycode])
        else:
            return None
    def mouseState(self):
        pos = sfml.Mouse.get_position()
      # Returns (pos_x, pos_y, Left State, Right State, Middle State)
        return (pos[0], pos[1], sfml.Mouse.is_button_pressed(0), sfml.Mouse.is_button_pressed(1), sfml.Mouse.is_button_pressed(2))

    def collides (obj):
        
        #returns "True" if obj overlaps with anything collideable
        def overlaps (hitBox1, hitBox2):
            if (hitBox1[0] <= hitBox2[2] and hitBox[0] >= hitBox2[0] and hitBox1[1] >= hitBox2[1] and hitBox1[1] <= hitBox2[3] or 
                hitBox1[0] <= hitBox2[2] and hitBox[0] >= hitBox2[0] and hitBox1[3] >= hitBox2[1] and hitBox1[1] <= hitBox2[3] or
                hitBox1[2] <= hitBox2[2] and hitBox[0] >= hitBox2[0] and hitBox1[1] >= hitBox2[1] and hitBox1[1] <= hitBox2[3] or
                hitBox1[2] <= hitBox2[2] and hitBox[0] >= hitBox2[0] and hitBox1[3] >= hitBox2[1] and hitBox1[1] <= hitBox2[3]):

                return True
        #enemies
        for entity in self.enemies:
            if entity != obj:
                objHitBox = (obj.pos.x, obj.pos.y, obj.pos[0] + obj.size[0], obj.pos[1] + obj.size[1])
                enemyHitBox = (entity.pos.x, entity.pos.y, entity.pos.x + entity.size.x, entity.pos.y + entity.size.y)
                if self.overlaps(objHitBox, enemyHitBox):
                    return True
            else:
                return False
                
                    
                
        #player
        #eventually walls

    def collided (obj):
        pass
        
