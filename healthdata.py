class HealthData:
    def __init__(self,max,regen):
        self.max = max
        self.current = max
        self.regen = regen
    
    def regenerate(self):
        if self.current == self.max:
            return False
        self.current += self.regen
        if self.current > self.max:
            self.current = self.max
        return True
