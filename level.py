# Class to wrap a single level and provide accessors
# Author - Nate Buck

import random

from room import Room

class Level:

    # Initializer override
    def __init__(self):
        # For use during generation, a list of rooms in the level
        self.rooms = []
        # The actual Tiles of the rooms
        self.tiles = []
        # Random enemy locations
        self.enemyLocations = []


    # Generate rooms for this Level
    def generate(self, levelSize = 1, roomSize = 1, heightVariance = 2.0, heightDropoff = 0.30, heightSmoothness = 4.0):
        # Determine number of rooms based on desired sizes
        minRooms = 0
        maxRooms = 0
        if levelSize == 0:
            if roomSize == 2:
                minRooms = 1
                maxRooms = 2
            elif roomSize == 1:
                minRooms = 2
                maxRooms = 3
            else:
                minRooms = 2
                maxRooms = 4
        elif levelSize == 1:
            if roomSize == 2:
                minRooms = 2
                maxRooms = 4
            elif roomSize == 1:
                minRooms = 3
                maxRooms = 6
            else:
                minRooms = 4
                maxRooms = 8
        else:
            if roomSize == 2:
                minRooms = 3
                maxRooms = 5
            elif roomSize == 1:
                minRooms = 4
                maxRooms = 8
            else:
                minRooms = 6
                maxRooms = 11

        # Determine room dimensions based on desired sizes
        minSize = 0
        maxSize = 0
        if roomSize == 2:
            minSize = 40
            maxSize = 55
        elif roomSize == 1:
            minSize = 25
            maxSize = 40
        else:
            minSize = 15
            maxSize = 25

        # Generate some rooms
        numRooms = 1#random.randint(minRooms, maxRooms)
        numMeshes = 0
        print("Rooms: " + str(numRooms))
        r = 0
        while r < numRooms:
            print("Room #" + str(r))
            newRoom = Room()
            newRoom.generateHeightMap( random.randint(minSize - 3, minSize + 3), random.randint(maxSize - 3, maxSize + 3),
                heightVariance, heightDropoff, heightSmoothness)
            newRoom.generatePassability()
            if (not newRoom.generateMeshes(numMeshes)):
                continue
            numMeshes += len(newRoom.meshes)

            self.rooms.append(newRoom)
            print("Room success! Great!")
            r += 1

        # Put the rooms in the level




        # Okay....just...have this part
        return self.rooms[0]



    # Connect the rooms of this Level

