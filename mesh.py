# Stores information about a continuous group of Tiles of the same height; the Tiles do not know what Mesh they are in
# Author - Nate Buck

from collections import deque

from tile import Tile

class Mesh: 

    # The bounds of this Mesh
    left = -1
    right = -1
    top = -1
    bottom = -1

    # Initializer override
    def __init__(self, seedTile, ID):
        # The first Tile contained by this Mesh; height will be -1 if this Mesh should not be expanded
        self.seedTile = seedTile
        # The height of this Mesh
        self.height = seedTile.height   
        # The Tiles contained in this Mesh; populated during Mesh.expand()
        self.tiles = []
        # The Tiles along the edge of this Mesh
        self.edgeTiles = []
        # The midpoint of this Mesh
        self.midpoint = [-1, -1]
        # The ID of this Mesh
        self.meshID = ID
        # The Meshes that this Mesh is connected to
        self.connectedMeshes = []

    # Expands a seeded, unexpanded Mesh fully
    def expand(self, tileMap):
        # Check if expansion can occur
        if (self.seedTile.height == -1):
            return

        # Create a queue of Tiles; deque is used for efficiency
        queue = deque()
        queue.append(self.seedTile)

        # During expansion, maintain the bounding box around this Mesh to get the midpoint later
        self.left = 1000000
        self.right = -1
        self.top = -1
        self.bottom = 10000000

        # Iterate through the queue, adding unexamined valid neighbors to the queue and adding queue members to this Mesh
        while len(queue) > 0:
            # Pop the front Tile and add it to this Mesh
            currTile = queue.popleft()
            # Don't add Tiles that have already been examined
            if (currTile.hasBeenExamined):
                continue

            # Update with the current Tile
            currTile.hasBeenExamined = True
            self.tiles.append(currTile)
            currTile.meshID = self.meshID
            x = currTile.position[0]
            y = currTile.position[1]

            # Add the current Tile's passable neighbors that are of the same height and are unexamined to the queue
            for m in [ a for a in range(-1, 2) if ( a + x >= 0 and a + x < len(tileMap) ) ]:
                for n in [ b for b in range(-1, 2) if ( b + y >= 0 and b + y < len(tileMap[x]) ) ]:
                    # Check the neighbor
                    neighbor = tileMap[x + m][y + n]
                    if (not neighbor.hasBeenExamined and neighbor.isPassable and neighbor.height == currTile.height):
                        queue.append(neighbor)
                    # If on a boundary (i.e. adjacent to an impassable Tile), add this Tile to the Mesh's edgeTiles
                    if (not neighbor.isPassable):
                        self.edgeTiles.append(tileMap[x][y])

            # Update the Mesh's bounding box
            self.left   = min(self.left,   currTile.position[0])
            self.right  = max(self.right,  currTile.position[0])
            self.top    = min(self.top,    currTile.position[1])
            self.bottom = max(self.bottom, currTile.position[1])

        # end queue

        # Flag seedTile to prevent re-expansion
        self.seedTile = Tile(-1, -1, -1, False)

        # Compute midpoint
        self.midpoint[0] = ( self.left + self.right ) / 2.0
        self.midpoint[1] = ( self.top + self.bottom ) / 2.0

    # end expand()
