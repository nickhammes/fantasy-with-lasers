# Class to store data for a tile in a level's map
# Author - Nate Buck

class Tile:

    # Initializer override
    def __init__(self, height, x, y, isPassable):
        # This tile's height, after being mapped to an integer value by its Room
        self.height = height
        # The [x, y] position of this Tile
        self.position = [x, y]
        # Whether or not this Tile is passable, determined by its Room
        self.isPassable = isPassable
        # Used in Room.generatePassability, indicates whether or not a Tile has been examined for creating Meshes
        self.hasBeenExamined = False
        # Whether or not this Tile is a ramp between multiple height values; assuming that it will go from this Tile's height up to height + 1
        self.isRamp = False
        # For funsies and probably for later reference, store the direction of this ramp; 0 = Right, 1 = Up...
        # The direction to go to go upwards
        self.rampDirection = -1
        # Gives the ID of the Mesh that this Tile belongs to, which should be unique if handled by the Level generating the Room that generated this Tile
        self.meshID = -1
