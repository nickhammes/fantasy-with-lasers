from healthdata import HealthData
from vector import Vector
import constants

class Enemy():

    def __init__(self, hpMax, hpRegen, armorMax, armorRegen, shieldMax, shieldRegen, x, y, spriteIndex):
        self.hp = HealthData(hpMax,hpRegen)

        self.armor = HealthData(armorMax,armorRegen)

        self.shield = HealthData(shieldMax,shieldRegen)

        self.status = []

        self.abilities = []

        #x,y position
        self.pos = Vector(x,y)
        self.enemyID = 0
        self.sprite = None
        self.spriteIndex = spriteIndex
    
    def step(self, session):
        if session.difficulty >= constants.HARD or not session.inCombat(self):
            self.hp.regenerate()
            self.armor.regenerate()
            self.shield.regenerate()
    
    def updateSprite (self):
        if self.sprite != None:
            self.sprite.position = (self.pos.x,self.pos.y)

DefaultEnemy = Enemy(10, 1, 10, 10, 10, 1, 640, 640, -1)
