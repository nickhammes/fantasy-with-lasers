from projectile import Projectile
from vector import Vector
from math import sqrt, atan2

class Laser:
    def __init__ (self,shooter,type,fireDelay,projSpeed,projSize,damage,attributes):
        self.shooter = shooter
        self.type = type
        self.fireRate = fireDelay
        self.fireRateTimer = 0
        self.projSpeed = projSpeed
        self.projSize = projSize
        self.damage = damage
        self.attributes = attributes
        self.proj = Projectile(shooter,attributes)
    
    def shoot (self,session):
        temp = self.proj.clone()
        session.addProjectile(temp,self.texture)
        return temp

class player():
    def __init__ (self, x, y, defTexture, textureIndex = 0):
        self.hpMax = 100.0
        self.hpCurrent = 100.0

        self.armor = 5.0
        self.shield = 5.0

        self.swordType = 'sword'  #denotes current weapon (sword, scythe, etc)
        self.swordSwingArc = 90
        self.swordSwingRate = 1.00
        self.swordDamage = 10.0
        self.swordRange = 0   #only applies to thrown swords (sworderangs)
        self.swordEffect = []

        self.laser = Laser(self,'pistol',30,20.0,1.0,10.0,[])
        
        self.status = []
        self.keyCount = 0
        
        #x,y position
        self.pos = Vector(x,y)
        self.facing = 0.0
        self.moveSpeed = 5
        self.defTexture = defTexture
        self.sprite = None
        self.textureIndex = textureIndex
    
    def step(self, session):
        self.updatePos(session)
        self.updateSprite()
        
        # check shooting
        if session.mouseState()[3]:
            temp = self.laser.shoot(session)
            theta = atan2(session.mouseState()[1]-self.pos.y,session.mouseState()[0]-self.pos.x)
            temp.setVelocity(theta,self.laser.projSpeed)
    
    def updatePos (self, session):
        horizontal = 0
        vertical = 0
        if session.keyState("KEY_LEFT") and not session.keyState("KEY_RIGHT"):
            horizontal = -1
        if session.keyState("KEY_RIGHT"):
            horizontal = 1
        if session.keyState("KEY_UP") and not session.keyState("KEY_DOWN"):
            vertical = -1
        if session.keyState("KEY_DOWN"):
            vertical = 1
        
        if horizontal == 0 and vertical == 0:
            return
        elif vertical == 0:
            self.pos += Vector(horizontal,0)*self.moveSpeed
        elif horizontal == 0:
            self.pos += Vector(0,vertical)*self.moveSpeed
        else:
            self.pos += Vector(horizontal,vertical)*(self.moveSpeed/sqrt(2))
    
    def getSprite(self):
        self.updateSprite()
        return self.sprite  
    
    def updateSprite(self):
        if self.sprite != None:
            self.sprite.position = (self.pos.x,self.pos.y)
    
    
