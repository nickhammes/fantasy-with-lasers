import sfml
from vector import Vector
from math import sin, cos

def spaces (int = 1):
    return "   " * int

def status (str):
    return "target.status.append('{}')".format(str)

class Projectile:
    startCollideCode = "def metacollide(self,target):"
    damageTarget = spaces(1) + "target.damage(self)"
    snippets = {
        "bleed" : spaces(1) + status("bleed"),
        "healthDrain" : spaces(1) + "self.shooter.heal(self.damage * self.healthDrainFraction)",
        "slow" : spaces(1) + status("slow"),
        "stun" : spaces(1) + status("stun"),
        "weaken" : spaces(1) + status("weaken")
    }
    
    def getSnippet(str):
        return snippets.get(str)
    
    def __init__ (self,shooter,texture,attributes=[]):
        self.texpath = texture
        self.shooter = shooter
        self._velocity = Vector(0,0)
        self._pos = Vector(0,0)
        self.projectileID = -1
        self.attributes = attributes
        
        code = [Projectile.startCollideCode,Projectile.damageTarget]
        for str in self.attributes:
            temp = Projectile.getSnippet(str)
            if temp is not None:
                code.append(temp)
        exec('\n'.join(code),self.__dict__)
    
    def clone (self):
        output = Projectile(None,None)
        output.__dict__ = self.__dict__
        return output
    
    def setSprite (self,texture):
        self.sprite = sfml.Sprite(texture)
    
    def setPosition (self, pos):
        self._pos = pos
    
    def setVelocity (self, angle, speed):
        self._velocity.x = speed * cos(angle)
        self._velocity.y = speed * sin(angle)
    
    def collide (self,other):
        if other == shooter:
            return
        self.metacollide(self,other)
    
    def step (self,session):
        if "homing" in self.attributes:
            pass
        else:
            self._pos += self._velocity
        
        print("derp")
        
        if self.sprite != None:
            self.sprite.position = (self._pos.x,self._pos.y)
        if session.collides(self):
            self.collide(session.collided(self))
            session.removeProjectile(self.projectileID)
            