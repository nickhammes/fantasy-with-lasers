class Vector:
    def __init__ (self, x = 0, y = 0):
        self.x = x
        self.y = y
    
    def __add__ (self, other):
        output = Vector(self.x, self.y)
        output.x += other.x
        output.y += other.y
        return output;
    
    def __sub__ (self, other):
        return self + (other * -1)
    
    def __mul__ (self, factor):
        output = Vector(self.x,self.y)
        output.x *= factor
        output.y *= factor
        return output
    
    def __truediv__ (self, factor):
        return self * (1/factor)
    
    def __iadd__ (self, other):
        self.x += other.x
        self.y += other.y
        return self
    
    def __isub__ (self, other):
        self.x -= other.x
        self.y -= other.y
        return self
    
    def __imul__ (self, factor):
        self.x *= factor
        self.y *= factor
        return self
    
    def __itruediv__ (self, factor):
        self.x /= factor
        self.y /= factor
        return self