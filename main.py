#!/usr/bin/env python3
import sfml
import random
from ai import *
from enemy import *
from session import Session

def main():
    # Prepatory Shenanigans
    ses = Session()
    ses.addEnemy(AIUnit(DefaultEnemy,AIMoveRandom()),"assets/sprites/lolcat.bmp")
    e1 = AIUnit(DefaultEnemy,AIMoveRandom())
    # Main Game Loop
    gamestate = True
    while gamestate:
        for event in ses.window.events:
            if type(event) is sfml.CloseEvent:
                gamestate = False
                ses.window.close()
        ses.step()

main()
