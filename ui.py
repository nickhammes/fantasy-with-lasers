__name__ = "ui"
import constants
import string
import sfml
class Overlay:
    def __init__(self):
        self.health = None
        self.hp = None
        self.status = None
        self.fontSize = 15
    def step(self, session):
        self.health = sfml.Text(str(session.pc.hpCurrent), constants.BASEFONT, self.fontSize)
        self.health.color = sfml.Color.RED
        self.health.position = (115, 0)
        self.hp = sfml.Text("HP: ", constants.BASEFONT, self.fontSize)
        self.hp.color = sfml.Color.RED
        self.hp.position = (45, 0)
        self.status = sfml.Text("Status: ", constants.BASEFONT, self.fontSize)
        self.status.color = sfml.Color.RED
        self.status.position = (45, 15)
        self.keys = sfml.Text(str(session.pc.keyCount), constants.BASEFONT, self.fontSize)
        self.keys.color = sfml.Color.RED
        self.keys.position = (115, 30)
        self.keyCount = sfml.Text("Keys: ", constants.BASEFONT, self.fontSize)
        self.keyCount.color = sfml.Color.RED
        self.keyCount.position = (45, 30)
        session.window.draw(self.health)
        session.window.draw(self.hp)
        session.window.draw(self.status)
        session.window.draw(self.keyCount)
        session.window.draw(self.keys)
