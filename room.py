# Simple class to store info for a single room in a level

import random
import math

class Room:

    # Height values of the room; will be square during generation but not necessarily afterwards
    heights = {}

    # x, y values of potential exits
    exits = {}

    # Generate a heightmap for this room, given dimensions
    def generateHeightMap(self, width, height):

        # Do nothing if widht or height is too small
        if width <= 3 or height <= 3:
            return self.heights

        # Define the range of height values to use
        minHeight = 0.0
        maxHeight = 10.0

        # Initialize the height map with random heights for fractal algorithm
        random.seed()
        self.heights = [[], [], []]
        for i in range(0, 3): # range() is [0, 3)
            self.heights[0].append( random.uniform(minHeight, maxHeight) );
            self.heights[1].append( random.uniform(minHeight, maxHeight) );
            self.heights[2].append( random.uniform(minHeight, maxHeight) );

        # Run fractal algorithm until width/height are satisfied
        currIteration = 0
        numIterations = math.floor( math.log( max(width, height) - 2, 2 ) ) # Each iteration increases map size by power of 2, starting with a 3 x 3 map
        print("numIterations is " + str(numIterations))

        for iteration in range(0, numIterations):

            currDim = len(self.heights)
            nextDim = 2 * currDim - 1

            # Insert new columns (x) of zeros
            for x in range(currDim - 1, 0, -1): # Working backward for nice indexing; the 0 is non-inclusive
                newCol = [] 
                for z in range(0, nextDim):
                    newCol.append(0)
                self.heights.insert(x, newCol)

            # Expand existing columns (now the even columns) with zeros
            for x in range(0, nextDim, 2): # nextDim is non-inclusive
                for y in range(currDim - 1, 0, -1):
                    self.heights[x].insert(y, 0)


            # Define parameters for height map generation
            # TODO: Allow these to be passed in as arguments? Can just default to these values
            variance = 2.0
            dropoff = 0.35
            smoothness = 2.9

            # Just square step: New values are treated as midpoints of lines and assigned values by average of endpoints, with variance; midpoints of squares use the four corners

            # Examine new points in old columns/rows, which will be the midpoints of lines
            for x in range(0, nextDim, 2):
                for y in range(1, nextDim, 2):
                    # Get average of endpoints
                    avg = ( self.heights[x][y - 1] + self.heights[x][y + 1] ) / 2.0
                    # Perturb by some amount dependent on parameters and current iteration number
                    avg += random.uniform(-1.0, 1.0) * variance * 1.0 / ( (dropoff * iteration) ** smoothness + 1.0 )
                    # Assign this value to the new point
                    self.heights[x][y] = avg;

                    # The above did the old row; now do the old column, assuming width = height which it should within scope of this function
                    # Get average of endpoints
                    avg = ( self.heights[y - 1][x] + self.heights[y + 1][x] ) / 2.0
                    # Perturb by some amount dependent on parameters and current iteration number
                    avg += random.uniform(-1.0, 1.0) * variance * 1.0 / ( (dropoff * iteration) ** smoothness + 1.0 )
                    # Assign this value to the new point
                    self.heights[y][x] = avg;


            # Examine new points exclusive to new columns/rows, which will be the midpoints of squares
            for x in range(1, nextDim, 2):
                for y in range(1, nextDim, 2):
                    # Get average of endpoints
                    avg = ( self.heights[x - 1][y - 1] + self.heights[x - 1][y + 1] + self.heights[x + 1][y - 1] + self.heights[x + 1][y + 1] ) / 4.0
                    # Perturb by some amount dependent on parameters and current iteration number
                    avg += random.uniform(-1.0, 1.0) * variance * 1.0 / ( (dropoff * iteration) ** smoothness + 1.0 )
                    # Assign this value to the new point
                    self.heights[x][y] = avg;


        # end height map generation

        # Clamp to min and max height values, then clamp to broad ranges for different "floor heights"
        # 0 - bottom 15%; 1 - 15-35%; 2 - 35-65%; 3 - 65-85%; 4 - top 15%
        for x in range(0, len(self.heights)):
            for y in range(0, len(self.heights[x])):
                # Clamp to min/maxHeight
                value = min( max( self.heights[x][y], minHeight ), maxHeight )
                # Clamp to floor height ranges
                if   ( value < 0.15 * (maxHeight - minHeight) - minHeight ):
                    self.heights[x][y] = 0
                elif ( value < 0.35 * (maxHeight - minHeight) - minHeight ):
                    self.heights[x][y] = 1
                elif ( value < 0.65 * (maxHeight - minHeight) - minHeight ):
                    self.heights[x][y] = 2
                elif ( value < 0.85 * (maxHeight - minHeight) - minHeight ):
                    self.heights[x][y] = 3
                else:
                    self.heights[x][y] = 4

        # Remove singleton holes, etc., by smoothing all tiles with less than 2 8-directional neighbors at its height
        # Must iterate both forward and backward to ensure smoothness

        # Debug! Print!
        print("\n\nGenerated:")
        for col in self.heights:
            print(col)

        # Forward
        for x in range(0, len(self.heights)):
            for y in range(0, len(self.heights[x])):
                # Prepare
                numNeighborsAtSameHeight = 0
                neighborHeights = []

                # Check all neighbors, counting the number of neighbors at this tile's height as well as the other heights that are found
                for m in [ a for a in range(-1, 2) if ( a + x >= 0 and a + x < len(self.heights) ) ]:
                    for n in [ b for b in range(-1, 2) if ( b + y >= 0 and b + y < len(self.heights) ) ]:
                        # Ignore (0, 0), as that is the tile being examined
                        if m == 0 and n == 0:
                            continue
                        # Check neighbor
                        if self.heights[x + m][y + n] == self.heights[x][y]:
                            numNeighborsAtSameHeight += 1
                        neighborHeights.append(self.heights[x + m][y + n])

                # If there are 2 or more neighbors at this height, continue
                if numNeighborsAtSameHeight >= 2:
                    continue
                # Otherwise, set this tile to have the height as is most common amongst its neighbors
                self.heights[x][y] = max( set(neighborHeights), key = neighborHeights.count );

        # Backward (repeats forward but iterates backward)
        for x in range(len(self.heights) - 1, -1, -1):
            for y in range(len(self.heights) - 1, -1, -1):
                numNeighborsAtSameHeight = 0
                neighborHeights = []

                for m in [ a for a in range(-1, 2) if ( a + x >= 0 and a + x < len(self.heights) ) ]:
                    for n in [ b for b in range(-1, 2) if ( b + y >= 0 and b + y < len(self.heights) ) ]:
                        if m == 0 and n == 0:
                            continue
                        if self.heights[x + m][y + n] == self.heights[x][y]:
                            numNeighborsAtSameHeight += 1
                        neighborHeights.append(self.heights[x + m][y + n])

                if numNeighborsAtSameHeight >= 2:
                    continue
                self.heights[x][y] = max( set(neighborHeights), key = neighborHeights.count );



        # Debug! Print!
        print("\n\nSmoothed:")
        for col in self.heights:
            print(col)

        # Reduce height map to desired width and height
        # NOTE: Indexed by (row, col)

        # Truncate rows (will actually be columns)
        actualHeightMap = self.heights[ math.floor(( len(self.heights) - width ) / 2.0) : len(self.heights) - math.ceil(( len(self.heights) - width ) / 2.0) ]

        # Truncate columns (will actually be rows)
        for x in range(0, len(actualHeightMap)):
            actualHeightMap[x] = actualHeightMap[x][math.floor(( len(self.heights) - height ) / 2.0) : len(self.heights) - math.ceil(( len(self.heights) - height ) / 2.0)]

        print("\n\nCropped:")
        for col in actualHeightMap:
            print(col)

    # end generateHeightMap





